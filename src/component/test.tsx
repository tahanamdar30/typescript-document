import React from "react";

type testProps = {
  name: string;
  age: number;
  isMarried: boolean;
};

export const Test = (props: testProps) => {
  return (
    <div>
      <h2>Welcome {props.name} !</h2>
      <h3>You're {props.age} Years Old</h3>
    </div>
  );
};

// //type
// let test: string = "taha";
// let number: number = 10;
// let un: undefined = undefined;
// let nu: null = null;
// let x: any = 50;
// let Y: boolean | string = true;
// Y = false;
// Y = "string";
// let array: Array<number> = [1, 2, 3];
// //function

// const someTest = (value1: number, value2: number): number => {
//   return value1 + value2;
// };

// interface Person {
//   fName: string;
//   lName: string;
//   age: number;
// }

// const func = (person: Person) => {
//   console.log(`myname is ${person.fName} ${person.lName}`);
// };

// let user1 = {
//   fName: "Taha",
//   lName: "Namdar",
//   age: 23,
// };
// func(user1);
